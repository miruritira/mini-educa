package minieduca.educa;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

@Component
public class EducaClient {
    private final HttpClient client;

    public EducaClient() {
        this.client = HttpClient.newHttpClient();
    }

    public JsonArray send(HttpRequest request) {
        try {
            HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString());
            return new Gson().fromJson(response.body(), JsonArray.class);
        } catch (IOException | InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
}
