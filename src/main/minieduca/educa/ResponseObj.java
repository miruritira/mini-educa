package minieduca.educa;

import java.util.ArrayList;

public class ResponseObj {
    private Boolean error;
    private String responseMsg;
    private ArrayList<ResponseDataDTO> data;

    public Boolean getError() {
        return error;
    }

    public void setError(Boolean error) {
        this.error = error;
    }

    public String getResponseMsg() {
        return responseMsg;
    }

    public void setResponseMsg(String responseMsg) {
        this.responseMsg = responseMsg;
    }

    public ArrayList<ResponseDataDTO> getData() {
        return data;
    }

    public void setData(ArrayList<ResponseDataDTO> data) {
        this.data = data;
    }
}
