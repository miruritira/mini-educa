package minieduca.educa;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;

@RestController
public class GetEducaDatosController {

    @GetMapping("/getDatosCentros")
    public ResponseEntity<ResponseObj> getDatosCentros(@RequestParam(required = false) String centros) {
        System.out.println(centros);
        ResponseObj response = new ResponseObj();
        if(centros == null || centros.equals("")){
            response.setError(true);
            response.setResponseMsg("Peticion incorrecta, centros obligatorios");
            return ResponseEntity.ok(response);
        }

        response.setError(false);
        String[] listaCentros = centros.split(",");

        Boolean existenCentrosConDatos = false;
        String centrosConDatos = "";
        ArrayList<ResponseDataDTO> listaCentrosDatos = new ArrayList<>();
        for(String centro : listaCentros){
            System.out.println(centro);
            //var ... manager.getdatoscentro(centro);
            if(centro.equals("Pamplona")){
                centrosConDatos.concat(centro).concat(",");
                existenCentrosConDatos = true;

                ResponseDataDTO centroData = new ResponseDataDTO();

                centroData.setCentro("Pamplona 123");
                centroData.setGrupo("Grupo 456");
                centroData.setOferta("Oferta 789");
                centroData.setNumAlumnos(26);

                listaCentrosDatos.add(centroData);

                response.setData(listaCentrosDatos);
            }
        }

        if(existenCentrosConDatos){
            response.setResponseMsg("Existen datos para: Pamplona");
        }else{
            response.setResponseMsg("Para los centros solicitados ".concat(centros).concat(" no hay datos disponibles"));
        }

        return ResponseEntity.ok(response);
    }

}