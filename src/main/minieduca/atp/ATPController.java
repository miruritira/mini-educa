package minieduca.atp;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ATPController {
    @GetMapping("/getAsignacionProfesores")
    public String getAsignacionProfesores(@RequestParam(value = "centros", required = false) String centros) {
        if(centros == null ||centros.isEmpty()){
            return "{Error: Peticion incorrecta, centros obligatorios}";
        }
        return "Hola desde ATP :)";
    }
}