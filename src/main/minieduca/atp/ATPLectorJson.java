package minieduca.atp;

import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;
import org.springframework.util.FileCopyUtils;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

@Component
public class ATPLectorJson {
    ResourceLoader resourceLoader;

    public ATPLectorJson(ResourceLoader resourceLoader) {
        this.resourceLoader = resourceLoader;
    }

    public String leer() {
        try {
            Resource resource = resourceLoader.getResource("classpath:disponibilidad_profesores.json");
            InputStream inputStream = resource.getInputStream();
            byte[] dataAsBytes = FileCopyUtils.copyToByteArray(inputStream);
            return new String(dataAsBytes, StandardCharsets.UTF_8);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
