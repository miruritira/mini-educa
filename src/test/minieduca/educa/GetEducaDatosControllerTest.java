package minieduca.educa;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.hamcrest.Matchers.equalTo;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class GetEducaDatosControllerTest {
    @Autowired
    private MockMvc mvc;

    @Test
    public void notifiesIfCentersWithNoData() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get("/getDatosCentros?centros=Burlada,Mutilva").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string(equalTo("{\"error\":false,\"responseMsg\":\"Para los centros solicitados Burlada,Mutilva no hay datos disponibles\",\"data\":null}")));
    }

    @Test
    public void getsErrorIfCentersParamNotPresent() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get("/getDatosCentros").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string(equalTo("{\"error\":true,\"responseMsg\":\"Peticion incorrecta, centros obligatorios\",\"data\":null}")));

        ResponseObj response = new ResponseObj();
        mvc.perform(MockMvcRequestBuilders.get("/getDatosCentros?centros=").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                //.andExpect(content().equals(response));
                .andExpect(content().string(equalTo("{\"error\":true,\"responseMsg\":\"Peticion incorrecta, centros obligatorios\",\"data\":null}")));
    }

    @Test
    public void getsDataForCentersWithData() throws Exception {
        ResponseObj response = new ResponseObj();
        mvc.perform(MockMvcRequestBuilders.get("/getDatosCentros?centros=Pamplona").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string(equalTo("{\"error\":false,\"responseMsg\":\"Existen datos para: Pamplona\",\"data\":[{\"centro\":\"Pamplona 123\",\"grupo\":\"Grupo 456\",\"oferta\":\"Oferta 789\",\"numAlumnos\":26}]}")));
    }
}
