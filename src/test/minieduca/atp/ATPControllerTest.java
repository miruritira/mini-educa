package minieduca.atp;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.hamcrest.Matchers.equalTo;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class ATPControllerTest {
    @Autowired
    private MockMvc mvc;

    @Test
    public void getsRegards() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get("/getAsignacionProfesores?centros=saludos").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string(equalTo("Hola desde ATP :)")));
    }

    @Test
    public void getsErrorWithoutRequestParameter() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get("/getAsignacionProfesores").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string(equalTo("{Error: Peticion incorrecta, centros obligatorios}")));
    }

    @Test
    public void getsErrorOnEmptyListRequest() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get("/getAsignacionProfesores?centros=").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string(equalTo("{Error: Peticion incorrecta, centros obligatorios}")));
    }
}
